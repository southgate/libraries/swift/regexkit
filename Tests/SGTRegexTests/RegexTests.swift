@testable import SGTRegex
import XCTest

final class RegexKitTests: XCTestCase {
    func testMatchesShouldReturnMatches() throws {
        let regex = try Regex(pattern: "[a-z]+")
        let input = "foo.bar.baz"

        let matches = regex.matches(in: input)

        XCTAssertEqual(matches.count, 3)
        XCTAssertEqual(matches[0].group(at: 0), "foo")
        XCTAssertEqual(matches[1].group(at: 0), "bar")
        XCTAssertEqual(matches[2].group(at: 0), "baz")
    }

    func testFirstMatchShouldReturnFirstMatch() throws {
        let regex = try Regex(pattern: "[a-z]+")
        let input = "foo.bar.baz"

        let match = regex.firstMatch(in: input)

        XCTAssertNotNil(match)
        XCTAssertEqual(match!.group(at: 0), "foo")
    }

    func testRegexMatchGroupsShouldReturnFirstGroup() throws {
        let regex = try Regex(pattern: "[a-z]+")
        let input = "foo.bar.baz"

        let match = regex.firstMatch(in: input)

        XCTAssertNotNil(match)
        XCTAssertEqual(match!.group, "foo")
        XCTAssertEqual(match!.group, match!.group(at: 0))
    }

    func testRegexMatchGroupsShouldReturnAllGroups() throws {
        let regex = try Regex(pattern: "^(?<foo>foo)\\.(?<bar>bar)\\.(?<bar2>baz)$")
        let input = "foo.bar.baz"

        let match = regex.firstMatch(in: input)
        XCTAssertNotNil(match)
        let groups = match!.groups

        XCTAssertEqual(groups.count, 4)
        XCTAssertEqual(groups[0], "foo.bar.baz")
        XCTAssertEqual(groups[1], "foo")
        XCTAssertEqual(groups[2], "bar")
        XCTAssertEqual(groups[3], "baz")
    }

    @available(iOS 11.0, *)
    @available(tvOS 11.0, *)
    @available(watchOS 4.0, *)
    @available(macOS 10.13, *)
    func testRegexMatchGroupWithNameShouldBeAbleToSelectByGroupName() throws {
        let regex = try Regex(pattern: "^(?<foo>foo)\\.(?<bar>bar)\\.(?<bar2>baz)$")
        let input = "foo.bar.baz"

        let match = regex.firstMatch(in: input)
        XCTAssertNotNil(match)
        let group1 = match!.group(withName: "foo")
        let group2 = match!.group(withName: "bar")
        let group3 = match!.group(withName: "bar2")

        XCTAssertNotNil(group1)
        XCTAssertNotNil(group2)
        XCTAssertNotNil(group3)
        XCTAssertEqual(group1!, "foo")
        XCTAssertEqual(group2!, "bar")
        XCTAssertEqual(group3!, "baz")
    }

    @available(iOS 11.0, *)
    @available(tvOS 11.0, *)
    @available(watchOS 4.0, *)
    @available(macOS 10.13, *)
    func testRegexMatchGroupWithNameShouldReturnNilIfNoGroupFound() throws {
        let regex = try Regex(pattern: "^(?<foo>foo)\\.(?<bar>bar)\\.(?<bar2>baz)$")
        let input = "foo.bar.baz"

        let match = regex.firstMatch(in: input)
        XCTAssertNotNil(match)
        let group = match!.group(withName: "bad_name")
        XCTAssertNil(group)
    }
}
