# Regex
Regex is a super simple wrapper library. As NSRegularExpression doesn't have the friendliest Swift API - this library wraps NSRegularExpression and exposes a nicer API.

## Examples
```swift
let regex = try Regex(pattern: "[a-z]+")
let input = "foo.bar.baz"
for match in regex.matches(in: input) {
    print(match.group(at: 0))
}
```

```swift
let regex = try Regex(pattern: "^(?<foo>foo)\\.(?<bar>bar)\\.(?<bar2>baz)$")
let input = "foo.bar.baz"

if let match = regex.firstMatch(in: input) {
    print(match.numberOfGroups)
    print(match.group(at: 0))
    print(match.group(at: 1))
    print(match.group(at: 2))
    print(match.group(at: 3))
    print("")
    
    print(match.group(withName: "foo"))
    print(match.group(withName: "bar"))
    print(match.group(withName: "bar2"))
}
```
