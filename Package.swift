// swift-tools-version: 6.0
import PackageDescription

let package = Package(
    name: "SGTRegex",
    platforms: [.iOS(.v14), .macCatalyst(.v14), .tvOS(.v14), .macOS(.v11), .watchOS(.v7), .visionOS(.v1)],
    products: [
        .library(name: "SGTRegex", targets: ["SGTRegex"])
    ],
    dependencies: [
        .package(path: "../SGTCore"),
        .package(url: "git@gitlab.com:southgate/libraries/spm/SwiftLintPlugins.git", from: "0.58.2")
    ],
    targets: [
        .target(name: "SGTRegex",
                dependencies: ["SGTCore"],
                plugins: [
                    .plugin(name: "SwiftLintBuildToolPlugin", package: "SwiftLintPlugins")
                ]),
        .testTarget(name: "SGTRegexTests",
                    dependencies: ["SGTRegex"],
                    plugins: [
                        .plugin(name: "SwiftLintBuildToolPlugin", package: "SwiftLintPlugins")
                    ])
    ]
)
