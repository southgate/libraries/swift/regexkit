import Foundation

// swiftlint:disable legacy_objc_type
/// An immutable representation of a compiled regular expression that you apply to Unicode strings.
public final class Regex: Sendable {
    private let nsRegularExpression: NSRegularExpression

    /// - Parameter pattern: The regular expression pattern.
    public init(pattern: String) throws {
        self.nsRegularExpression = try NSRegularExpression(pattern: pattern)
    }

    /// Returns the regular expression pattern.
    public var pattern: String {
        nsRegularExpression.pattern
    }

    /// Returns the number of capture groups in the regular expression.
    public var numberOfCaptureGroups: Int {
        nsRegularExpression.numberOfCaptureGroups
    }

    /// Returns an array containing all the matches of the regular expression in the string.
    public func matches(in string: String) -> [RegexResult] {
        let nsString = string as NSString
        let results = nsRegularExpression.matches(in: string, range: nsString.range)
        return results.map { result in
            return RegexResult(nsString: nsString, result: result)
        }
    }

    /// Returns the first match of the regular expression within the specified range of the string.
    public func firstMatch(in string: String) -> RegexResult? {
        let nsString = string as NSString
        let result = nsRegularExpression.firstMatch(in: string, range: nsString.range)
        guard let result else {
            return nil
        }
        return RegexResult(nsString: nsString, result: result)
    }

    /// Returns a string by adding backslash escapes as necessary to protect any characters that would match as pattern
    /// metacharacters.
    public static func escapedPattern(for string: String) -> String {
        NSRegularExpression.escapedPattern(for: string)
    }
}

extension NSString {
    fileprivate var range: NSRange {
        NSRange(location: 0, length: length)
    }
}
// swiftlint:enable legacy_objc_type
