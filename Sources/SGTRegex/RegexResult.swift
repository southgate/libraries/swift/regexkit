import Foundation
import SGTCore

// swiftlint:disable legacy_objc_type
/// An occurrence of textual content found during the analysis of a block of text found when matching a regular
/// expression.
public final class RegexResult: Sendable {
    private let string: String
    @SGTMutexWrapped private var result: NSTextCheckingResult

    /// - Parameters:
    ///   - nsString: The NSString
    ///   - result: The raw NSTextCheckingResult
    public init(nsString: NSString, result: NSTextCheckingResult) {
        self.string = nsString as String
        self._result = SGTMutex(result)
    }

    /// Returns the number of ranges.
    public var numberOfGroups: Int {
        result.numberOfRanges
    }

    /// Returns the first group
    public var group: String {
        group(at: 0) ?? ""
    }

    /// Returns the group at a given index
    public func group(at index: Int) -> String? {
        let range = result.range(at: index)
        if range.length == 0 {
            return nil
        }
        return nsString.substring(with: range)
    }

    /// Returns the group with a given name
    @available(iOS 11.0, *)
    @available(tvOS 11.0, *)
    @available(watchOS 4.0, *)
    @available(macOS 10.13, *)
    public func group(withName name: String) -> String? {
        let range = result.range(withName: name)
        if range.length == 0 {
            return nil
        }
        return nsString.substring(with: range)
    }

    /// Returns all groups
    public var groups: [String] {
        var groups: [String] = []
        for i in 0..<numberOfGroups {
            guard let group = self.group(at: i) else {
                continue
            }
            groups.append(group)
        }
        return groups
    }

    private var nsString: NSString {
        return string as NSString
    }
}
// swiftlint:enable legacy_objc_type
